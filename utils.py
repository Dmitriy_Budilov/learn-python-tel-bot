from emoji import emojize
from telegram import ReplyKeyboardMarkup, KeyboardButton

from random import choice

from config import USER_EMOJI

def get_user_emoj(user_data):
    if 'emoj' in user_data:
        return user_data['emoj']
    else:
        user_data['emoj'] = emojize(choice(USER_EMOJI), use_aliases=True)
        return user_data['emoj']

def get_keyboard():
    contact_button = KeyboardButton('Контактные данные', request_contact=True)
    location_button = KeyboardButton('Геолокация',request_location=True)
    my_keyboard = ReplyKeyboardMarkup([
                                        ['Прислать котика', 'Сменить аватар'],
                                        [contact_button, location_button]
                                        ], resize_keyboard=True)
    return my_keyboard
