from utils import get_keyboard, get_user_emoj
import ephem

from datetime import datetime as dt
from random import choice
from glob import glob
import logging

def list_of_planets_maker():
    '''
        Формирование списка планет.
        Из списка убраны Луна и Солнце
    '''
    planets = []
    for val in ephem._libastro.builtin_planets():
        if val[1] == 'Planet':
            planets.append(val[2])
    return planets[:-2]

def greet_user(bot, update, user_data):
    '''
        Сообщение о вызове /start.
    '''
    emoj = get_user_emoj(user_data)
    user_data['emoj'] = emoj 
    text = f'Hi {emoj}'
    update.message.reply_text(text, reply_markup=get_keyboard())

def talk_to_me(bot, update, user_data):
    '''
        Функция возвращает текст переданный пользователем.
    '''
    emoj = get_user_emoj(user_data)
    user_text = 'Hi {} {}, you write {}!'.format(update.message.chat.first_name,
                                                 emoj,
                                                 update.message.text)
    logging.info('User: %s, Chat_id: %s, Message: %s', update.message.chat.username,
                                                    update.message.chat.id,
                                                    update.message.text)
    update.message.reply_text(user_text, reply_markup=get_keyboard())

def give_me_planet(bot, update, user_data):
    '''
        Функция получает имя планеты от пользователя и возвращает имя созвездия в котором оно находится.
    '''
    list_of_planets = list_of_planets_maker()
    planet = update.message.text
    planet = planet.split()[1]
    
    if planet in list_of_planets: # Если планета в списке планет - передаём её ephem.
        planet_now = getattr(ephem, planet)(dt.now())
        constellation = ephem.constellation(planet_now)
        # Формируем строку ответа.
        response = '{planet} is now in the constellation {constellation}.'.format(planet=planet, constellation=constellation[1])
        update.message.reply_text(response, reply_markup=get_keyboard())
    else: # Если планеты нет в списке планет - это не планета.
        # Формируем строку ответа.
        response = '{} is not a planet.'.format(planet)
        update.message.reply_text(response, reply_markup=get_keyboard())

def send_cat_picture(bot, update, user_data):
    '''
        Функция получаетсписок картинок из папки /images и
        отправляте в telegram по запросу команды /cat
    '''
    image_list = glob('images/cat*.jpeg')
    cat_picture = choice(image_list)
    bot.send_photo(chat_id=update.message.chat_id, photo=open(cat_picture, 'rb'), reply_markup=get_keyboard())

def get_contact(bot, update, user_data):
    print(update.message.contact)
    update.message.reply_text('Спасибо {}!'.format(get_user_emoj(user_data)), reply_markup=get_keyboard())

def get_location(bot, update, user_data):
    print(update.message.location)
    update.message.reply_text('Спасибо {}!'.format(get_user_emoj(user_data)), reply_markup=get_keyboard())

def change_avatar(bot, update, user_data):
    if 'emoj' in user_data:
        del user_data['emoj']
    emoj = get_user_emoj(user_data)
    update.message.reply_text('Готово: {}'.format(emoj), reply_markup=get_keyboard())
